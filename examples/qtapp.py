from PyQt6 import QtWidgets
from ui.mainwindow import MainWindow


app = QtWidgets.QApplication([])

window = MainWindow(None)
window.show()

app.exec()
