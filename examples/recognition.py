from model import VADCapture, DeepSpeech
from halo import Halo


model = DeepSpeech(model_path='resources/models',
                   scorer_path='deepspeech_scorer.scorer',
                   auto_generate_stream=True)
spinner = Halo(spinner='dot')


for device in VADCapture.get_input_devices():
    print(f'{device["index"]:<8}{device["name"]}')

device_index = int(input('Select your device: '))

print('Ctrl C to exit')
with VADCapture(device_index=device_index, input_rate=16000, aggressiveness=2) as vad:
    for frame in vad.vad_collector(ratio=0.5):
        if frame is not None:
            spinner.start()
            model.feed(frame)
            continue
        spinner.stop()
        print(f'Recognized: {model.finish_stream()}')
