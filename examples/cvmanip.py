from threading import Thread
from model import (
    VADCapture,
    DeepSpeech,
    CameraCapture,
    VirtualCamera
)
from PIL import ImageFont

model = DeepSpeech(model_path='resources/models',
                   scorer_path='deepspeech_scorer.scorer',
                   auto_generate_stream=True)
font = ImageFont.truetype(R'C:\Windows\Fonts\century.ttf', 20)
virt_cam = VirtualCamera(width=640, height=480, fps=60, font=font)
# shared across threads. One reader one writer.
text = ''


def audio_thread():
    with VADCapture(1, 16000, 2) as vad:
        for frame in vad.vad_collector(ratio=0.5):
            if frame is not None:
                model.feed(frame)
                continue
            text = model.finish_stream()
            if text:
                globals()['text'] = text
            print(f'Recognized: {text}')


Thread(target=audio_thread).start()

with CameraCapture() as camera:
    for frame in camera:
        virt_cam.feed_subtitle(frame[1], text)
