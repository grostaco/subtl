import pyaudio
import numpy as np
import collections
from queue import Queue
from typing import (
    Any,
    Iterator,
    Mapping,
    TypedDict,
    Iterable,
    Optional,
    cast,
)
import numpy.typing as npt
from scipy import signal
from more_itertools import ilen
import webrtcvad

__all__ = ["AudioCapture", "VADCapture"]


class Devices(TypedDict):
    defaultHighInputLatency: float
    defaultHighOutputLatency: float
    defaultLowInputLatency: float
    defaultLowOutputLatency: float
    defaultSampleRate: float
    hostApi: int
    index: int
    maxInputChannels: int
    maxOutputChannels: int
    name: str
    structVersion: int


class _PaHostInfo(TypedDict):
    index: int
    structVersion: int
    type: int
    name: str
    deviceCount: int
    defaultInputDevice: int
    defaultOutputDevice: int


class AudioCapture:
    FORMAT = pyaudio.paInt16
    RATE_PROCESS = 16000
    CHANNELS = 1
    BLOCKS_PER_SECOND = 50
    audio: Optional[pyaudio.PyAudio] = None

    def __init__(self, device_index: int, input_rate: int):
        self.device_index = device_index
        self.input_rate = input_rate
        self.sample_rate = AudioCapture.RATE_PROCESS
        self.block_size = AudioCapture.RATE_PROCESS // AudioCapture.BLOCKS_PER_SECOND
        self.block_size_input = self.input_rate // AudioCapture.BLOCKS_PER_SECOND

        self.buffer_queue: Queue[bytes] = Queue()
        self.pa = pyaudio.PyAudio()

        self.stream = self.pa.open(format=AudioCapture.FORMAT,
                                   channels=AudioCapture.CHANNELS,
                                   rate=self.input_rate,
                                   input=True,
                                   frames_per_buffer=self.block_size_input,
                                   stream_callback=self.stream_cb)

    def stream_cb(self, in_bytes: Optional[bytes], frame_count: int, time_info: Mapping[str, float], status: int):
        if in_bytes:
            self.buffer_queue.put(in_bytes)
        return None, pyaudio.paContinue

    def read(self):
        return self.buffer_queue.get()

    def resample(self, data: bytes, input_rate: int) -> bytes:
        data_16: npt.NDArray[np.uint8] = np.frombuffer(data, np.int16)
        resample_size = data_16.shape[0] // input_rate * self.RATE_PROCESS
        resample = cast(npt.NDArray[np.uint8],
                        signal.resample(data_16, resample_size))
        resample_16 = cast(npt.NDArray[np.int16], np.array(resample, np.int16))
        return resample_16.tobytes()

    def read_resampled(self):
        return self.resample(data=self.read(),
                             input_rate=self.input_rate)

    @property
    def frame_duration_ms(self):
        return 1000 * self.block_size // self.sample_rate

    def __enter__(self):
        self.stream.start_stream()
        return self

    def __exit__(self, exc_type: Any, exc_val: Any, exc_tb: Any):
        self.stream.stop_stream()
        self.stream.close()
        self.pa.terminate()

    @staticmethod
    def get_devices(host_api_index: int = 0) -> Iterable[Devices]:
        if AudioCapture.audio is None:
            AudioCapture.audio = pyaudio.PyAudio()

        audio = AudioCapture.audio
        info = cast(
            _PaHostInfo, audio.get_host_api_info_by_index(host_api_index))
        devices = (cast(Devices, audio.get_device_info_by_host_api_device_index(0, i))
                   for i in range(info['deviceCount']))
        return devices

    @staticmethod
    def get_input_devices(host_api_index: int = 0) -> Iterator[Devices]:
        return filter(lambda device: device['maxInputChannels'] > 0, AudioCapture.get_devices(host_api_index))

    @staticmethod
    def get_output_devices(host_api_index: int = 0) -> Iterator[Devices]:
        return filter(lambda device: device['maxOutputChannels'] > 0, AudioCapture.get_devices(host_api_index))


class VADCapture(AudioCapture):
    def __init__(self, device_index: int, input_rate: int, aggressiveness: int = 3):
        super().__init__(device_index, input_rate)
        self.vad = webrtcvad.Vad(aggressiveness)

    def __iter__(self):
        while True:
            if self.input_rate == AudioCapture.RATE_PROCESS:
                yield self.read()
            else:
                yield self.read_resampled()

    def __enter__(self):
        super().__enter__()
        return self

    def __exit__(self, exc_type: Any, exc_val: Any, exc_tb: Any):
        super().__exit__(exc_type, exc_val, exc_tb)

    def vad_collector(self, padding_ms: int = 300, ratio: float = 0.75, frames: Optional[Iterable[bytes]] = None) -> Iterator[Optional[bytes]]:
        frames = frames or iter(self)
        num_padding_ms = padding_ms // self.frame_duration_ms
        ring_buffer: collections.deque[tuple[bytes, bool]] = collections.deque(
            maxlen=num_padding_ms)
        triggered = False

        if frames is None:
            return

        for frame in frames:
            if len(frame) < 640:
                return

            is_speech = cast(bool, self.vad.is_speech(frame, self.sample_rate))
            ring_buffer.append((frame, is_speech))
            if not triggered:
                num_voiced = ilen(f for f, speech in ring_buffer if speech)
                if num_voiced > ratio * cast(float, ring_buffer.maxlen):
                    triggered = True
                    for f, _ in ring_buffer:
                        yield f
                    ring_buffer.clear()
            else:
                yield frame
                num_unvoiced = ilen(
                    f for f, speech in ring_buffer if not speech)
                if num_unvoiced > ratio * cast(float, ring_buffer.maxlen):
                    triggered = False
                    yield None
                    ring_buffer.clear()
