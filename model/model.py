import os
import numpy as np
import numpy.typing as npt
from deepspeech import (
    Model,
    Stream,
)
from typing import (
    Optional,
    cast,
)


class DeepSpeech:
    def __init__(self, model_path: str, scorer_path: str, auto_generate_stream: bool = False):
        if os.path.isdir(model_path):
            scorer_path = os.path.join(model_path, scorer_path)
            model_path = os.path.join(model_path, 'output_graph.pbmm')

        self.model = Model(model_path)
        self.model.enableExternalScorer(scorer_path)
        self.stream: Optional[Stream] = None
        self.auto_generate_stream = auto_generate_stream

    def generate_stream(self):
        self.stream = self.model.createStream()

    def feed(self, frame: bytes):
        if self.stream is None:
            if self.auto_generate_stream:
                self.generate_stream()
            else:
                raise ValueError(
                    'Stream was not created, did you forget to call a createStream()?')

        if self.stream:
            self.stream.feedAudioContent(
                cast(npt.NDArray[np.uint8], np.frombuffer(frame, np.int16)))

    def finish_stream(self) -> str:
        if self.stream is None:
            raise ValueError(
                'Stream was not created, did you forget to call a createStream()?')

        text = cast(str, self.stream.finishStream())
        self.stream = None
        return text
