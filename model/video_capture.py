import cv2
import pyvirtualcam
import numpy.typing as npt
import numpy as np
from typing import Any, Iterator, Optional
from PIL import (
    Image,
    ImageFont,
    ImageDraw,
)


class CameraCapture:
    def __init__(self, index: int = 0):
        self.capture: cv2.VideoCapture = cv2.VideoCapture(index)

    def __iter__(self) -> Iterator[tuple[bool, npt.NDArray[np.uint8]]]:
        while True:
            yield self.capture.read()

    def __enter__(self):
        return self

    def __exit__(self, *args: Any):
        self.capture.release()


class VirtualCamera(pyvirtualcam.Camera):
    def __init__(self, width: int, height: int, fps: float, font: ImageFont.FreeTypeFont, **kwargs: Any):
        super().__init__(width=width, height=height, fps=fps, **kwargs)
        self.buffer: Optional[np.ndarray[Any, np.dtype[np.uint8]]] = None
        self.font = font

    def feed_subtitle(self, frame: npt.NDArray[np.uint8], text: str):

        img = Image.fromarray(frame)
        draw = ImageDraw.Draw(img)
        width, height = draw.textsize(text, self.font)
        draw.text(((self.width - width) // 2, self.height - 2 *
                  height), text, (255, 255, 255), font=self.font)
        self.buffer = np.array(img)
        self.update()

    def update(self):
        if self.buffer is not None:
            self.send(self.buffer)
