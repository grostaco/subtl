from PyQt6.QtCore import QDir
from .mainwindow import MainWindow
from .backend import Backend

QDir.addSearchPath('icons', 'resources/icons/')


__all__ = ['MainWindow', 'Backend']
