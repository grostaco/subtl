from PyQt6 import QtGui
from PyQt6.QtCore import (
    QAbstractListModel,
    QDate,
    QDateTime,
    QModelIndex,
    Qt,
)
from typing import Optional, Any
from bisect import bisect_left, bisect_right
from typing import NamedTuple
from PyQt6.QtCore import QDateTime


__all__ = ['TodoList', 'TodoModel', 'DateTodoModel']


TodoRecord = NamedTuple(
    'TodoRecord', [('date', QDateTime), ('content', str), ('completed', bool)])


tick = QtGui.QImage('icons:tick.png')


class TodoList:
    def __init__(self) -> None:
        self.todos: list[TodoRecord] = []

    def query_record(self, min_date: QDateTime, max_date: QDateTime):
        keys = tuple(record.date for record in self)
        left = bisect_left(keys, min_date)
        right = bisect_left(keys, max_date)
        if right < len(keys) and keys[right] == max_date:
            right += 1
        return self.todos[left:right]

    def add_record(self, dt: QDateTime, content: str, completed: bool = False):
        if TodoRecord(dt, content, completed) not in self.todos:
            index = bisect_right(tuple(record.date for record in self), dt)
            self.todos.insert(index, TodoRecord(dt, content, completed))

    def complete_record(self, index: int):
        record = self.todos[index]
        self.todos[index] = TodoRecord(
            record.date, record.content, not record.completed)

    def remove_record(self, index: int):
        del self.todos[index]

    def __len__(self):
        return len(self.todos)

    def __iter__(self):
        return iter(self.todos)

    def __getitem__(self, index: int):
        return self.todos[index]


class TodoModel(QAbstractListModel):
    def __init__(self, todos: Optional[TodoList] = None, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self.todos: TodoList = todos or TodoList()

    def data(self, index: QModelIndex, role: Optional[int] = None):
        dt, content, completed = self.todos[index.row()]
        if role == Qt.ItemDataRole.DisplayRole:
            return f'{content:<24}{dt.toString("dd/MM/yyyy h:mm ap")}'
        elif role == Qt.ItemDataRole.DecorationRole:
            if completed:
                return tick

    def rowCount(self, parent: Optional[QModelIndex] = None):
        return len(self.todos)


class DateTodoModel(QAbstractListModel):
    def __init__(self, current_date: QDate, todos: TodoList, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self._todos: TodoList = todos
        self.current_date = current_date

    @property
    def todos(self):
        year, month, day = self.current_date.year(
        ), self.current_date.month(), self.current_date.day()
        return self._todos.query_record(
            QDateTime(year, month, day, 0, 0, 0), QDateTime(year, month, day, 0, 0, 0).addDays(1))

    def data(self, index: QModelIndex, role: Optional[int] = None):
        dt, content, completed = self.todos[index.row()]
        if role == Qt.ItemDataRole.DisplayRole:
            return f'{content:<24}{dt.toString("dd/MM/yyyy h:mm ap")}'
        elif role == Qt.ItemDataRole.DecorationRole:
            if completed:
                return tick

    def rowCount(self, parent: Optional[QModelIndex] = None):
        return len(self.todos)
