from typing import cast, Optional, TYPE_CHECKING
from PyQt6.QtWidgets import QFileDialog, QWidget
from PyQt6.QtCore import Qt, pyqtBoundSignal
from ui.uic import Ui_Settings

if TYPE_CHECKING:
    from ui.backend import Backend


class Settings(QWidget, Ui_Settings):
    def __init__(self, parent: Optional[QWidget] = None, flags: Qt.WindowType = ...,
                 backend: Optional['Backend'] = None) -> None:
        QWidget.__init__(self, parent=parent)
        Ui_Settings.__init__(self)
        self.setupUi(self)

        cast(pyqtBoundSignal, self.aggressiveSlider.valueChanged).connect(
            self.aggressivenessChanged)
        cast(pyqtBoundSignal, self.volumeSlider.valueChanged).connect(
            self.volumeChanged
        )
        cast(pyqtBoundSignal, self.fontChangeButton.pressed).connect(
            self.changeFont
        )
        self.fileDialogue = QFileDialog(self)

        if backend:
            self.audioComboBox.addItems(
                list(device['name'] for device in backend.get_input_devices()))
        else:
            self.audioComboBox.addItem('Backend disabled')
            self.audioComboBox.setDisabled(True)

    @property
    def input_device(self) -> int:
        return self.audioComboBox.currentIndex()

    def aggressivenessChanged(self, value: int):
        self.aggressiveCounter.setText(str(value))

    def volumeChanged(self, value: int):
        self.volumeCounter.setText(str(value))

    def changeFont(self):
        self.fileDialogue.setAcceptMode(
            QFileDialog.AcceptMode.AcceptOpen)
        if self.fileDialogue.exec():
            file, *_ = self.fileDialogue.selectedFiles()
            self.fontEdit.setText(file)
