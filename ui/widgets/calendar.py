from typing import Optional, cast
from PyQt6.QtCore import QDate, Qt, pyqtBoundSignal
from PyQt6.QtWidgets import QWidget
from ui.uic.calendar import Ui_Calendar
from ui.models import TodoList, DateTodoModel


class Calendar(QWidget, Ui_Calendar):
    def __init__(self, todos: TodoList, parent: Optional[QWidget] = None, flags: Qt.WindowType = ...) -> None:
        QWidget.__init__(self, parent=parent)
        Ui_Calendar.__init__(self)
        self.setupUi(self)

        self.model = DateTodoModel(QDate(), todos=todos)
        self.todoView.setModel(self.model)

        cast(pyqtBoundSignal, self.calendarWidget.clicked).connect(
            self.update_date)
        self.todos = todos

    def update_view(self):
        cast(pyqtBoundSignal, self.model.layoutChanged).emit()

    def update_date(self, date: QDate):
        self.model.current_date = date
        self.update_view()
