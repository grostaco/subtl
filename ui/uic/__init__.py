from .calendar import Ui_Calendar
from .mainwindow import Ui_MainWindow
from .settings import Ui_Settings

__all__ = ['Ui_Calendar', 'Ui_MainWindow', 'Ui_Settings']
