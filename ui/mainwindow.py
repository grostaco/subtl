import pickle
from typing import cast, Optional, TYPE_CHECKING
from PyQt6.QtCore import QDate, QDateTime, pyqtBoundSignal
from PyQt6.QtGui import QIcon, QPixmap
from PyQt6.QtWidgets import QFileDialog, QMainWindow, QMessageBox, QStatusBar
from ui.uic import Ui_MainWindow
from ui.models import TodoModel
from ui.widgets import Calendar, Settings

if TYPE_CHECKING:
    from .backend import Backend


class MainWindow(QMainWindow, Ui_MainWindow):
    def __init__(self, backend: Optional['Backend'] = None):
        QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)
        self.model = TodoModel()
        self.setting = Settings(backend=backend)
        self.calendar = Calendar(self.model.todos)

        if backend is None:
            print(
                'WARNING: Backend disabled, some SubTL\'s functions will not be available.')
        self.backend = backend

        self.todoView.setModel(self.model)
        self.todoTimeEdit.setDateTime(
            QDateTime(QDate.currentDate().year(), 1, 1, 0, 0, 0))
        self.todoTimeEdit.setDisplayFormat('MM/dd/yyyy h:mm ap')

        self.addButton.setStatusTip('Add a task')
        self.deleteButton.setStatusTip('Remove a task')
        self.completeButton.setStatusTip('Mark a task as complete')

        cast(pyqtBoundSignal, self.addButton.pressed).connect(self.add)
        cast(pyqtBoundSignal, self.deleteButton.pressed).connect(self.delete)
        cast(pyqtBoundSignal, self.completeButton.pressed).connect(self.complete)
        cast(pyqtBoundSignal, self.settingsButton.pressed).connect(
            self.showSettings)
        cast(pyqtBoundSignal, self.calendarButton.pressed).connect(
            self.showCalendar)

        cast(pyqtBoundSignal, self.actionCalendar.triggered).connect(
            self.showCalendar)
        cast(pyqtBoundSignal, self.action_Settings.triggered).connect(
            self.showSettings)
        cast(pyqtBoundSignal, self.actionSave.triggered).connect(self.save)
        cast(pyqtBoundSignal, self.actionLoad.triggered).connect(self.load)
        cast(pyqtBoundSignal, self.actionStart.triggered).connect(self.start)

        self.setStatusBar(QStatusBar(self))
        self.setWindowIcon(QIcon('icons:address-book--pencil.png'))

        self.fileDialogue = QFileDialog(self)

    def add(self):
        text = self.todoEdit.text()
        dt = self.todoTimeEdit.dateTime()
        if text:
            self.model.todos.add_record(dt, text)
            cast(pyqtBoundSignal, self.model.layoutChanged).emit()
            self.todoEdit.setText('')
            self.calendar.update_view()

    def delete(self):
        indices = self.todoView.selectedIndexes()
        if indices:
            index = indices[0]
            self.model.todos.remove_record(index.row())
            self.calendar.update_view()
            cast(pyqtBoundSignal, self.model.layoutChanged).emit()
            self.todoView.clearSelection()

    def complete(self):
        indices = self.todoView.selectedIndexes()
        index = None
        if indices:
            index = indices[0]
        if index is None:
            return

        row = index.row()
        self.model.todos.complete_record(row)

        cast(pyqtBoundSignal, self.model.dataChanged).emit(index, index)
        self.calendar.update_view()
        self.todoView.clearSelection()

    def showSettings(self):
        self.setting.show()

    def showCalendar(self):
        self.calendar.show()

    def save(self):
        self.fileDialogue.setAcceptMode(QFileDialog.AcceptMode.AcceptSave)
        if self.fileDialogue.exec():
            file, *_ = self.fileDialogue.selectedFiles()
            pickle.dump(self.model.todos, open(file, 'wb'))

    def start(self):
        if self.backend is None:
            message_box = QMessageBox(parent=self)
            icon = QIcon()
            icon.addPixmap(QPixmap(
                'resources/icons/exclamation-button.png'), QIcon.Mode.Normal, QIcon.State.Off)
            message_box.setWindowIcon(icon)
            message_box.setWindowTitle('Error!')
            message_box.setText('Backend is not set!')
            message_box.show()
            return

        if self.actionStart.isChecked():
            self.backend.start()
        else:
            self.backend.stop()

    def load(self):
        self.fileDialogue.setAcceptMode(QFileDialog.AcceptMode.AcceptOpen)
        if self.fileDialogue.exec():
            file, *_ = self.fileDialogue.selectedFiles()
            self.model.todos.todos.clear()
            for record in pickle.load(open(file, 'rb')):
                self.model.todos.add_record(
                    record.date, record.content, record.completed)
            cast(pyqtBoundSignal, self.model.layoutChanged).emit()
            self.calendar.update_view()
