from PIL.ImageFont import FreeTypeFont
from model import AudioCapture, VADCapture, VirtualCamera, DeepSpeech
from model.audio_capture import Devices
from typing import Iterator, Optional
from threading import Thread

from model.video_capture import CameraCapture


class Backend:
    def __init__(self) -> None:
        self.vad_capture: Optional[VADCapture] = None
        self.vad_thread: Optional[Thread] = None
        self.virt_cam: Optional[VirtualCamera] = None
        self.capt: Optional[CameraCapture] = None
        self.model: Optional[DeepSpeech] = None
        self.current_caption: Optional[str] = None
        self.should_stop: bool = False

    def set_vad_capture(self, device_index: int, input_rate: int):
        self.vad_capture = VADCapture(device_index, input_rate)

    def set_virtual_camera(self, width: int, height: int, fps: int, font: FreeTypeFont):
        self.virt_cam = VirtualCamera(width, height, fps, font)

    def set_model(self, model_path: str, scorer_path: str, auto_generate_stream: bool = False):
        self.model = DeepSpeech(model_path, scorer_path, auto_generate_stream)

    def set_camera(self, camera_index: int):
        self.capt = CameraCapture(index=camera_index)

    def generate_caption(self):
        if self.vad_thread is not None:
            raise ValueError('A VAD thread has already been started')

        # closure fun

        def _generate_caption():
            if self.vad_capture is None:
                raise ValueError(
                    'VAD capture is not set, call set_vad_capture to set vad capture')
            if self.model is None:
                raise ValueError(
                    'Model is not set, call set_model to set model')
            for frame in self.vad_capture.vad_collector(ratio=0.5):
                if frame is not None:
                    self.model.feed(frame)
                    continue
                self.current_caption = self.model.finish_stream()
                print(self.current_caption)

        self.vad_thread = Thread(target=_generate_caption, daemon=True)
        self.vad_thread.start()

    def start(self):
        if self.vad_thread is None:
            raise ValueError(
                'The VAD thread has not started, call generate_caption')

        def _virt_thread():
            if self.virt_cam is None:
                raise ValueError(
                    'No virtual camera set, call set_virtual_camera to set virtual camera')
            if self.capt is None:
                raise ValueError('No camera capture set, call set_')
            for frame in self.capt:
                if self.should_stop:
                    self.should_stop = False
                    return
                if self.current_caption:
                    self.virt_cam.feed_subtitle(frame[1], self.current_caption)
        self.virt_thread = Thread(target=_virt_thread, daemon=True)
        self.virt_thread.start()

    def stop(self):
        self.should_stop = True

    def get_input_devices(self) -> Iterator[Devices]:
        return AudioCapture.get_input_devices()
