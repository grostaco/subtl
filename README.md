# SubTL
A meeting manager with voice transcription using Python's
OpenCV and Mozilla's deepspeech made as a university project.


## Requirements
`Python` 3.9+
## Installation
Install SubTL's dependencies
```console
username@hostname:~$ apt get portaudio19-dev 
username@hostname:~$ python3 -m pip install -r requirements.txt
```
Due to issues with PyPi and PyAudio not providing a cp39 wheel, see [here](https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyaudio) 
for windows PyAudio's wheel. For linux distributions, consult the internet or use your package manager.

To get mozilla's pre-trained model, visit [DeepSpeech's github](https://github.com/mozilla/DeepSpeech).

Put your model into `models/`, the model folder should look like
```
models
├── deepspeech_scorer.scorer
└── output_graph.pbmm
```

## Usage
Run Python scripts from `examples/` to see example usages of SubTL.

For the main module, do
```
username@hostname:~$ python3 -m main
```
To run the main SubTL program.

# Attribution

- Icons provided by [Yusuke Kamiyamane](p.yusukekamiyamane.com). Licensed under a [Creative Commons Attribution 3.0 License](http://creativecommons.org/licenses/by/3.0/).