PY = python
PYUIC = pyuic6

UI_FILE := $(patsubst resources/uic/%.ui, ui/uic/%.py, $(wildcard resources/uic/*.ui))

.PHONY: all generate_ui qtapp cvmanip

all: main.py
	$(PY) $<

qtapp: examples/qtapp.py $(UI_FILE)
	$(PY) $<

cvmanip: examples/cvmanip.py
	$(PY) $<

$(UI_FILE): ui/uic/%.py: resources/uic/%.ui
	$(PYUIC) $< -o $@

generate_ui: $(UI_FILE)
