# Development Environment
## Platform
Both Windows and Linux are supported. The following console snippets are from an ubuntu terminal

## Python
Install Python and the appropriate libraries
```console
username@hostname:~/projects/subtl$ cd
username@hostname:~$ apt update
username@hostname:~$ apt upgrade
username@hostname:~$ apt intall python3.9 python3.9-venv python3.9-dev portaudio19-dev
```
For Windows, get PyAudio's wheel from [here](https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyaudio)

Create the virtual environment
```console
username@hostname:~$ mkdir .venv
username@hostname:~$ python3.9 -m venv .venv/subtl
```
Load the virtual environment
```console
username@hostname:~$ source .venv/subtl/bin/activate
```
Your console should look as follows
```console
(subtl) username@hostname:~$
```
Install SubTL's dependencies
```console
(subtl) username@hostname:~$ cd projects/subtl
(subtl) username@hostname:~/projects/subtl$ pip install -r requirements.txt
(subtl) username@hostname:~/projects/subtl$ pip install pyaudio
```
For Windows user
```console
(subtl) Z:\projects\subtl> pip install PyAudio-0.2.11-cp39-cp39-win_amd64.whl
```

## Makefile

SubTL uses make to generate python files from ui forms. If you are using windows, you can download them [here](http://gnuwin32.sourceforge.net/packages/make.htm). For Linux, consult your local package manager.

To generate UI files
```console
make generate_ui
``` 